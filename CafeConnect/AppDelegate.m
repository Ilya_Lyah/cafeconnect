//
//  AppDelegate.m
//  CafeConnect
//
//  Created by Ilya Lyah on 14.10.14.
//  Copyright (c) 2014 Orangesoft. All rights reserved.
//

#import "AppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>

// 706954536049051 - app id
// 6a1cdf65388e37fd384c20219777ffa2 - app secret

@interface AppDelegate ()

@end

@implementation AppDelegate

#pragma mark - UIApplicationDelegate protocols

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
}

@end
