//
//  ViewController.m
//  CafeConnect
//
//  Created by Ilya Lyah on 14.10.14.
//  Copyright (c) 2014 Orangesoft. All rights reserved.
//

#import "ViewController.h"
#import <FacebookSDK/FacebookSDK.h>

@interface ViewController ()

@end

@implementation ViewController

#pragma mark - UIViewController's methods

- (void)loadView
{
    [super loadView];
    
    FBLoginView *loginView = [[FBLoginView alloc] init];
    loginView.center = self.view.center;
    [self.view addSubview:loginView];
}

@end
